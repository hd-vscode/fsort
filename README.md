# FSort

Sort file contents based on custom rules and regular expressions.

## Features

Provides a command to sort the contents of a file. Sorting is based on *rule configurations*, which can be defined by the user. The extension defines various default configurations.

### Command Palette

Open the command palette (e.g. `ctrl+shift+p`) and search for the `FSort: Sort ...` command. When invoked like this the user is prompted to select a rule configuration.

### Task

The command `fsort.sort` might be invoked as `task` though a target file path and rule configuration name must be provided as arguments.

## Extension Settings

This extension contributes the following settings:

* `fsort.ruleSet`: An object defining available rule configurations.

A configuration has following properties:

* `expressions`: Array of regular expressions defining the sort order of file contents. Matches with the first expression are placed above matches with the second expression and so on. Any remaining content is placed at the end.
* `fallbacks`: Array of fallback sorting mechanisms. Available options are `"alphanumeric-asc"`, `"alphanumeric-desc"`, `"length-asc"`, `"length-desc"`.
* `evalOrder`: Defines the order in which the expressions are evaluated - either `"bottom-up"` (default) or `"top-down"`.

## Known Issues

None yet.

## Release Notes

### 1.0.0-alpha

Fully working prototype:

* `sort` command
* `ruleSet` configuration
* defaut configurations
  * `txt-an-asc`
  * `txt-an-desc`
  * `txt-len-asc`
  * `txt-len-desc`
  * `css-bem`
* evaluation orders
  * `bottom-up`
  * `top-down`
* fallback mechanisms
  * `alphanumeric-asc`
  * `alphanumeric-desc`
  * `length-asc`
  * `length-desc`
