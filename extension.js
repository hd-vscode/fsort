const vscode = require('vscode');

/**
 * Comparator map used for 'fallback' sorting. Supported values are:
 * - alphanumeric-asc: Alphanumeric order ascending
 * - alphanumeric-desc: Alphanumeric order descending
 * - length-asc: Text length ascending
 * - length-desc: Text length descending
 * - none: No secondary sorting (default)
 * 
 * An undefined 'fallback' property will behave the same as setting
 * it to 'none'.
 */
const comparator_map = {
	"alphanumeric-asc": (a, b) => a.localeCompare(b),
	"alphanumeric-desc": (a, b) => b.localeCompare(a),
	"length-asc": (a, b) => a.length - b.length,
	"length-desc": (a, b) => b.length - a.length,
	"none": () => 0
};

/**
 * Iterator map used to determine the order at which expressions
 * are evaluated. Supported values are:
 * - bottom-up: Match expressions from bottom to the top (default)
 * - top-down: Match expressions from the top to bottom
 * 
 * An undefined 'evalOrder' property will behave the same as setting
 * it to 'bottom-up'.
 */
const evalOrder_map = {
	"bottom-up": n => ({ has: () => n > 0, next: () => --n }),
	"top-down": n => { let i = 0; return { has: () => i < n, next: () => i++ }; }
}

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
	let disposable = vscode.commands.registerCommand("fsort.sort", async (filePath, ruleName) => {
		let set = vscode.workspace.getConfiguration("fsort.ruleSet");

		if (filePath === undefined) {
			getOpenFile().then(file => {
				if (ruleName === undefined) {
					getRuleName(set).then(rule => sortBy(file, set.get(rule)));
				} else {
					sortBy(file, set.get(ruleName));
				}
			}).catch(reason => {
				vscode.window.showErrorMessage("FSort: " + reason);
			});
		}
	});

	context.subscriptions.push(disposable);
}

/**
 * Sorts the given 'file' on the users file system according to the
 * rules specified by the 'rule'-object.
 * 
 * @param {string} file
 * @param {object} rule
 */
function sortBy(file, rule) {
	let file_uri = vscode.Uri.file(file);

	vscode.workspace.fs.readFile(file_uri).then(content => {
		let rule_exprs = rule.expressions;
		let rule_flbks = rule.fallbacks === undefined ? [] : rule.fallbacks;
		let rule_evord = rule.evalOrder === undefined ? "bottom-up" : rule.evalOrder;

		// Constructs a sequential comparator for all fallbacks (using recursion).
		let build_cmprt = (flbks) => {
			if (flbks.length > 0) {
				let curr_cmprt = comparator_map[flbks[0]];

				if (curr_cmprt === undefined) {
					throw "Unsupported fallback sorting '" + flbks[0] + "'";
				}

				let next_cmprt = build_cmprt(flbks.slice(1));

				return (a, b) => {
					let r = curr_cmprt(a, b);
					return r != 0 ? r : next_cmprt(a, b);
				};
			} else {
				return comparator_map["none"];
			}
		};

		var flbk_cmprt;

		try {
			flbk_cmprt = build_cmprt(rule_flbks);
		} catch (error) {
			vscode.window.showErrorMessage("FSort: " + error);
			return;
		}

		let content_string = new TextDecoder().decode(content);
		let arr = [];

		// iterate over expressions in the order specified by 'evalOrder'
		let build_it = evalOrder_map[rule_evord];

		if (build_it === undefined) {
			vscode.window.showErrorMessage("FSort: Unsupported evaluation order '" + rule_evord + "'");
			return;
		}

		let exprs_it = build_it(rule_exprs.length);

		while (exprs_it.has()) {
			let i = exprs_it.next();
			let matches = content_string.matchAll(rule_exprs[i]);

			for (const match of matches) {
				let d = match.index;
				let l = match[0].length;

				// add matched string to arr
				arr.push({ text: match[0], weight: i, index: d });

				// replace matched chars with newline chars ('\n') in content_string
				// (so they won't match again with different weights)
				let left = d > 0 ? content_string.substring(0, d) : "";
				let right = (d + l) < content_string.length ? content_string.substring(d + l, content_string.length) : "";
				content_string = left + '\n'.repeat(l) + right;
			}
		}

		// sort matched strings based on their weights and join them
		// together into a single string
		let content_sorted = arr
			.sort((a, b) => {
				if (a.weight != b.weight) {
					return a.weight - b.weight;
				}

				let r = flbk_cmprt(a.text, b.text);
				return r != 0 ? r : a.index - b.index;
			}).map(v => v.text).join("\n");

		// add remainding file content at end of sorted content
		content_sorted += '\n' + content_string
			.replace(/\r/g, "")
			.replace(/[\n]+/g, '\n').trim();

		// write resulting string to file
		vscode.workspace.fs.writeFile(file_uri, new TextEncoder()
			.encode(content_sorted))
			.then(
				() => vscode.window.showInformationMessage("FSort: Sorted content of '" + file + "'."),
				(reason) => vscode.window.showErrorMessage("FSort: " + reason));
	});
}

/**
 * Asks the user for a specific rule from 'fsort.ruleSet' and returns it.
 */
function getRuleName(set) {
	let s = Object.keys(set).slice(4); // skip 'has' 'get' 'update' 'inspect'
	return vscode.window.showQuickPick(s);
}

/**
 * Retrieves the path to the currently opened file.
 */
async function getOpenFile() {
	let editor = vscode.window.activeTextEditor;

	if (editor === undefined) {
		throw "No textfile opened!";
	}

	return editor.document.uri.fsPath;
}

// This method is called when your extension is deactivated
function deactivate() { }

module.exports = {
	activate,
	deactivate
}
